const mongoose = require('mongoose');

const alunoSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true    
    },
    age: Number
})

const Aluno = mongoose.model('Aluno', alunoSchema);
module.exports = Aluno;