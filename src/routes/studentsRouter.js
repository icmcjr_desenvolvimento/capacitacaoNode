import express from 'express';
import Aluno from '../models/aluno';

const studentsRouter = express.Router();

studentsRouter.get('/', async (req, res) => {
  const alunos = await Aluno.find();
  res.status(200).send(alunos);
});

studentsRouter.get('/:id', async (req, res) => {
  const aluno = await Aluno.findById(req.params.id);
  res.status(200).send(aluno);
});

studentsRouter.post('/', async (req, res) => {
  const aluno = new Aluno({
    name: req.body.name,
    age: req.body.age
  });
  await aluno.save();

  res.status(201).send(aluno);
});

studentsRouter.put('/:id', async (req, res) => {
  const aluno = await Aluno.findById(req.params.id);
  aluno.name = req.body.name;
  aluno.age = req.body.age;

  await aluno.save();

  res.status(200).send(aluno);
});

studentsRouter.delete('/:id', async (req, res) => {
  const aluno = await Aluno.findById(req.params.id);

  await aluno.remove();

  res.status(200).send('O aluno foi removido com sucesso !' + aluno.name);
});

export default studentsRouter;
