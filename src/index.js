import express from 'express';
import bodyParser from 'body-parser';

import cors from 'cors';

import studentsRouter from './routes/studentsRouter';
require('./db/connection');

const server = express();

server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());
server.use(cors());


server.use('/alunos', studentsRouter);

server.listen(80, () => {
  console.log('RODANDO');
});
